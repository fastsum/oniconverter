
/*
 * Created: 09-05-2018
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <iomanip>
#include <iostream>

#include "lib/builders/builder_utilities.hpp"
#include "lib/builders/config_io_builder.hpp"
#include "lib/cli/parse_cmd.hpp"
#include "lib/config-io/openqcd/openqcd_io.hpp"
#include "lib/config-io/qdp/qdp-io.hpp"
#include "lib/types/lattice_iterator.hpp"
#include "lib/utility/mpi_instance.hpp"

namespace oni = fastsum::oniconverter;

int main(int argc, char **argv)
{
  auto program_parameters = oni::parse_program_arguments(argc, argv);

  oni::MPI_Instance mpi_instance{argc, argv};
  auto latopt = oni::lattice_options{};
  latopt.lattice_size = program_parameters.lattice_size;

  auto reader = oni::create_reader(
      oni::to_filetype(program_parameters.reader.type_string));
  auto writer = oni::create_writer(
      oni::to_filetype(program_parameters.writer.type_string));

  reader->initialise(latopt);
  reader->read_from_file(program_parameters.reader.filename, latopt);

  writer->initialise(latopt);

  oni::su3_matrix gauge_link;
  for (auto it = oni::lattice_link_iterator{latopt.lattice_size}; !it.done(); ++it) {
    reader->get(it, gauge_link);
    writer->put(it, gauge_link);
  }

  std::cout << std::setprecision(16);
  std::cout << "From config plaquette: " << reader->plaquette() << "\n"
            << "To config plaquette:   " << writer->plaquette() << "\n";

  writer->write_to_file(program_parameters.writer.filename);
}
