
/*
 * Created: 29-05-2018
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "lib/types/lattice_iterator.hpp"
#include "gmock/gmock.h"

namespace oni = fastsum::oniconverter;

TEST(Lattice_Site_Iterator, All_Indices)
{
  auto lattice_size = oni::lattice_site_index{4, 6, 2, 8};
  auto lit = oni::lattice_site_iterator{lattice_size};

  ASSERT_FALSE(lit.done());

  for (auto t = 0u; t < lattice_size[0]; ++t) {
    for (auto x = 0u; x < lattice_size[1]; ++x) {
      for (auto y = 0u; y < lattice_size[2]; ++y) {
        for (auto z = 0u; z < lattice_size[3]; ++z) {

          ASSERT_FALSE(lit.done());
          ASSERT_THAT(
              oni::lattice_site_index({lit.t(), lit.x(), lit.y(), lit.z()}),
              ::testing::ElementsAre(t, x, y, z));

          ++lit;
        }
      }
    }
  }

  ASSERT_TRUE(lit.done());

  lit.reset();
  ASSERT_FALSE(lit.done());
  ASSERT_THAT(
      oni::lattice_site_index({lit.t(), lit.x(), lit.y(), lit.z()}),
      ::testing::ElementsAre(0, 0, 0, 0));
}

TEST(Lattice_Link_Iterator, All_Indices)
{
  auto lattice_size = oni::lattice_site_index{4, 6, 2, 8};
  auto lit = oni::lattice_link_iterator{lattice_size};

  ASSERT_FALSE(lit.done());

  for (auto t = 0u; t < lattice_size[0]; ++t) {
    for (auto x = 0u; x < lattice_size[1]; ++x) {
      for (auto y = 0u; y < lattice_size[2]; ++y) {
        for (auto z = 0u; z < lattice_size[3]; ++z) {
          for (auto mu = 0u; mu < 4; ++mu) {

            ASSERT_FALSE(lit.done());
            ASSERT_THAT(oni::lattice_link_index(
                            {lit.t(), lit.x(), lit.y(), lit.z(), lit.mu()}),
                        ::testing::ElementsAre(t, x, y, z, mu));

            ++lit;
          }
        }
      }
    }
  }

  ASSERT_TRUE(lit.done());

  lit.reset();
  ASSERT_FALSE(lit.done());
  ASSERT_THAT(
      oni::lattice_link_index({lit.t(), lit.x(), lit.y(), lit.z(), lit.mu()}),
      ::testing::ElementsAre(0, 0, 0, 0, 0));
}
