
/*
 * Created: 29-05-2018
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "lib/config-io/qdp/qdp-io.hpp"
#include "lib/utility/mpi_instance.hpp"
#include "gtest/gtest.h"
#include <numeric>
#include <random>
#include <cstdio>

namespace oni = fastsum::oniconverter;

template <typename G>
oni::su3_matrix random_su3_matrix(G &g)
{
  static auto dist = std::normal_distribution<double>{0.0, 1.0};
  auto result = oni::su3_matrix{};

  for (auto i = 0u; i < result.size(); ++i)
    for (auto j = 0u; j < result[i].size(); ++j)
      result[i][j] = {dist(g), dist(g)};

  return result;
}

TEST(ConfigIO_QDP, Write_Read)
{
  auto parms = oni::lattice_options{};
  parms.lattice_size = {8, 8, 8, 8};

  std::mt19937 mt_enigne{12345};

  // Generate some dummy data
  auto volume =
      std::accumulate(parms.lattice_size.begin(), parms.lattice_size.end(), 1,
                      [](std::size_t n, std::size_t m) { return n * m; });

  ASSERT_EQ(volume, 8 * 8 * 8 * 8);

  auto lattice_elements = std::vector<oni::su3_matrix>(4 * volume);

  ASSERT_EQ(lattice_elements.size(), 4 * volume);

  std::generate(lattice_elements.begin(), lattice_elements.end(),
                [&mt_enigne]() { return random_su3_matrix(mt_enigne); });

  // Initialise writer
  oni::QDP_Configuration_Writer writer{};
  writer.initialise(parms);

  auto lit = oni::lattice_link_iterator{parms.lattice_size};
  for (auto eit = lattice_elements.begin(); eit != lattice_elements.end();
       ++eit, ++lit) {
    ASSERT_FALSE(lit.done());
    writer.put(lit, *eit);
  }

  ASSERT_TRUE(lit.done());

  writer.write_to_file("tmp_file.dat");

  // Reset and initialise reader
  oni::QDP_Configuration_Reader reader{};
  reader.initialise(parms);
  ASSERT_NO_THROW(reader.read_from_file("tmp_file.dat", parms));

  lit.reset();
  oni::su3_matrix buffer{};
  for (auto eit = lattice_elements.begin(); eit != lattice_elements.end();
       ++eit, ++lit) {
    ASSERT_FALSE(lit.done());
    reader.get(lit, buffer);
    ASSERT_EQ(*eit, buffer);
  }

  ASSERT_TRUE(lit.done());

  std::remove("tmp_file.dat");
}
