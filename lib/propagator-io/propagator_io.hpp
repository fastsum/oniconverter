
/*
 * Created: 08-05-2018
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef CONFIG_IO_HPP
#define CONFIG_IO_HPP

#include "lib/types/lattice_options.hpp"
#include "lib/types/lattice_iterator.hpp"
#include "lib/types/propagator_matrix.hpp"

namespace fastsum {
namespace oniconverter {

class Propagator_IO
{
public:
  virtual double square_norm() const = 0;

  virtual void initialise(lattice_options const &opt) = 0;
  virtual void finalise(lattice_options const &opt) = 0;

  virtual ~Propagator_IO(){};
};

class Propagator_Reader : public virtual Propagator_IO
{
public:
  virtual void read_from_file(std::string const &, lattice_options &opt) = 0;
  virtual void get(lattice_site_iterator const &, propagator_matrix &) = 0;

  virtual ~Propagator_Reader(){};
};

class Propagator_Writer : public virtual Propagator_IO
{
public:
  virtual void write_to_file(std::string const &) = 0;
  virtual void put(lattice_site_iterator const &, propagator_matrix const &) = 0;

  virtual ~Propagator_Writer(){};
};

} // namespace oniconverter
} // namespace fastsum

#endif /* CONFIG_IO_HPP */
