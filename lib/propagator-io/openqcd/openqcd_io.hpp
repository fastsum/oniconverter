
/*
 * Created: 25-05-2018
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef OPENQCD_PROPAGATOR_IO_HPP
#define OPENQCD_PROPAGATOR_IO_HPP

#include "lib/propagator-io/propagator_io.hpp"

namespace fastsum {
namespace oniconverter {

class openQCD_Propagator_IO : public virtual Propagator_IO
{
public:
  double square_norm() const override;

  void finalise(lattice_options const &) override{};

  virtual ~openQCD_Propagator_IO(){};
};

class openQCD_Propagator_Reader : public Propagator_Reader,
                                  public openQCD_Propagator_IO
{
public:
  void initialise(lattice_options const &) override{};

  void read_from_file(std::string const &, lattice_options &opt) override;
  void get(lattice_site_iterator const &link_id,
           propagator_matrix &output_matrix) override;

  virtual ~openQCD_Propagator_Reader(){};
};

class openQCD_Propagator_Writer : public Propagator_Writer,
                                  public openQCD_Propagator_IO
{
public:
  void initialise(lattice_options const &opt) override;

  void write_to_file(std::string const &) override;
  void put(lattice_site_iterator const &link_id,
           propagator_matrix const &input_matrix) override;

  virtual ~openQCD_Propagator_Writer(){};
};

} // namespace oniconverter
} // namespace fastsum

#endif /* OPENQCD_PROPAGATOR_IO_HPP */
