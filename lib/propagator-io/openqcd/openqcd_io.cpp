
/*
 * Created: 08-05-2018
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "openqcd_io.hpp"
#include "lib/utility/openqcd/instance.hpp"
#include <cstdio>
#include <openqcd-propagator.hpp>
#include <openqcd.hpp>

namespace fastsum {
namespace oniconverter {

namespace {

/* Parse the lattice size of a configuration from an openQCD configuration file
 * */
lattice_site_index parse_lattice_size_from_infile(std::string const &filename)
{
  std::array<int, 4> parsing_array;
  std::FILE *ifs;

  ifs = std::fopen(filename.c_str(), "rb");

  if (ifs == nullptr)
    throw std::runtime_error{"Unable to open file \"" + filename +
                             "\" for reading."};

  std::fread(parsing_array.data(), sizeof(int), 4, ifs);
  std::fclose(ifs);

  return {static_cast<std::size_t>(parsing_array[0]),
          static_cast<std::size_t>(parsing_array[1]),
          static_cast<std::size_t>(parsing_array[2]),
          static_cast<std::size_t>(parsing_array[3])};
}

/* The superindex of pos (t,x,y,z) for the OpenQCD storage */
int openqcd_site_index(int t, int x, int y, int z)
{
  return openqcd::ipt()[z + openqcd::L3() *
                                (y + openqcd::L2() * (x + openqcd::L1() * t))];
}

void copy_matrix(openqcd::spinor_dble **from, propagator_matrix &to, int index)
{
  for (auto i = 0; i < 4; ++i) {
    for (auto j = 0; j < 4; ++j) {
      for (auto a = 0; a < 3; ++a) {
        auto ptr = reinterpret_cast<double *>(from[3 * i + a] + index);
        for (auto b = 0; b < 3; ++b) {
          to[i][j][a][b] = {*(ptr + 2 * (3 * j + b)),
                            *(ptr + 2 * (3 * j + b) + 1)};
        }
      }
    }
  }
}

void copy_matrix(propagator_matrix const &from, openqcd::spinor_dble **to,
                 int index)
{
  for (auto i = 0; i < 4; ++i) {
    for (auto j = 0; j < 4; ++j) {
      for (auto a = 0; a < 3; ++a) {
        auto ptr = reinterpret_cast<double *>(to[3 * i + a] + index);
        for (auto b = 0; b < 3; ++b) {
          *(ptr + 2 * (3 * j + b)) = from[i][j][a][b].real();
          *(ptr + 2 * (3 * j + b) + 1) = from[i][j][a][b].imag();
        }
      }
    }
  }
}

} // namespace

double openQCD_Propagator_IO::square_norm() const
{
  if (!openQCD_Instance::is_geometry_set())
    throw std::runtime_error{
        "Cannot measure the plaquette before OpenQCD is initialised"};

  auto prop = openqcd::propagator::propagator_field();

  auto norm = 0.0;
  for (auto i = 0; i < 12; ++i) {
    norm += openqcd::linalg::norm_square_dble(openqcd::VOLUME(), 0, prop[i]);
  }

  return norm;
}

void openQCD_Propagator_Reader::get(lattice_site_iterator const &site_id,
                                    propagator_matrix &output_matrix)
{
  copy_matrix(
      openqcd::propagator::propagator_field(), output_matrix,
      openqcd_site_index(site_id.t(), site_id.x(), site_id.y(), site_id.z()));
}

void openQCD_Propagator_Reader::read_from_file(std::string const &filename,
                                               lattice_options &opt)
{
  auto parsed_size = parse_lattice_size_from_infile(filename);

  if (opt.lattice_size[0] == 0) {
    opt.lattice_size = parsed_size;
  } else if (opt.lattice_size != parsed_size) {
    throw std::runtime_error{"Propagator file lattice size different to the "
                             "one set as input parameter"};
  }

  openQCD_Instance::set_lattice_size(opt);
  openqcd::propagator::import_propagator(filename.c_str());

  if (!openqcd::lattice::check_bc(64.0 * DBL_EPSILON) or
      openqcd::flags::query_flags(UD_IS_CLEAN) == 0)
    throw std::runtime_error{"Unexpeced boundary values for OpenQCD config"};
}

void openQCD_Propagator_Writer::initialise(lattice_options const &opt)
{
  openQCD_Instance::set_lattice_size(opt);
}

void openQCD_Propagator_Writer::put(lattice_site_iterator const &site_id,
                                    propagator_matrix const &input_matrix)
{
  copy_matrix(
      input_matrix, openqcd::propagator::propagator_field(),
      openqcd_site_index(site_id.t(), site_id.x(), site_id.y(), site_id.z()));
}

void openQCD_Propagator_Writer::write_to_file(std::string const &filename)
{
  if (!openqcd::lattice::check_bc(64.0 * DBL_EPSILON) or
      openqcd::flags::query_flags(UD_IS_CLEAN) == 0)
    throw std::runtime_error{"Unexpeced boundary values for OpenQCD config"};

  openqcd::propagator::export_propagator(filename.c_str());
}

} // namespace oniconverter
} // namespace fastsum
