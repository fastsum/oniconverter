
/*
 * Created: 10-05-2018
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef QDP_IO_HPP
#define QDP_IO_HPP

#include "lib/propagator-io/propagator_io.hpp"
#include <memory>

namespace fastsum {
namespace oniconverter {

class QDP_Instance;

class QDP_Propagator_IO : public virtual Propagator_IO
{
public:
  QDP_Propagator_IO();

  double square_norm() const override;
  void finalise(lattice_options const &) override{};

  virtual ~QDP_Propagator_IO();

protected:
  class Propagator_Implementation;
  std::unique_ptr<Propagator_Implementation> propagator_impl_;

private:
  QDP_Instance& qdp_instance_;
};

class QDP_Propagator_Reader : public Propagator_Reader,
                                 public QDP_Propagator_IO
{
public:
  QDP_Propagator_Reader();

  void initialise(lattice_options const &) override{};

  void read_from_file(std::string const &, lattice_options &opt) override;
  void get(lattice_site_iterator const &link_id,
           propagator_matrix &output_matrix) override;

  virtual ~QDP_Propagator_Reader();
};

class QDP_Propagator_Writer : public Propagator_Writer,
                                 public QDP_Propagator_IO
{
public:
  QDP_Propagator_Writer();

  void initialise(lattice_options const &opt) override;

  void write_to_file(std::string const &) override;
  void put(lattice_site_iterator const &link_id,
           propagator_matrix const &input_matrix) override;

  virtual ~QDP_Propagator_Writer();
};

} // namespace oniconverter
} // namespace fastsum

#endif /* QDP_IO_HPP */
