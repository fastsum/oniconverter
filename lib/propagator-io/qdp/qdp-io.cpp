
/*
 * Created: 10-05-2018
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "qdp.h"

#include "lib/utility/make_unique.hpp"
#include "lib/utility/qdp/qdp-instance.hpp"
#include "lib/utility/xml_parser.hpp"
#include "qdp-io.hpp"

namespace fastsum {
namespace oniconverter {

using QDP_Colour_Matrix = QDP::PColorMatrix<QDP::RComplex<double>, 3>;
using QDP_Propagator_Matrix = QDP::PSpinMatrix<QDP_Colour_Matrix, 4>;

namespace {

const std::string xml_lattice_size_tag = "latt_size";
const std::string scidac_file_header_tag = "scidac-file-xml";

lattice_site_index parse_latt_size_string(std::string latt_size)
{
  std::istringstream iss{latt_size};
  lattice_site_index size;

  iss >> size[1] >> size[2] >> size[3] >> size[0];

  return size;
}

/* Parse the lattice size of a configuration from a QDP configuration file */
lattice_site_index parse_lattice_size_from_xml_header(std::string filename)
{
  auto ifs = std::unique_ptr<FILE, void (*)(FILE *)>{
      std::fopen(filename.c_str(), "r"), [](FILE *fptr) { std::fclose(fptr); }};

  auto lime_reader = std::unique_ptr<LimeReader, void (*)(LimeReader *)>{
      limeCreateReader(ifs.get()),
      [](LimeReader *reader) { limeDestroyReader(reader); }};

  while (limeReaderNextRecord(lime_reader.get()) == LIME_SUCCESS) {
    auto read_type = std::string{limeReaderType(lime_reader.get())};
    auto read_bytes = limeReaderBytes(lime_reader.get());

    if (read_type == scidac_file_header_tag) {
      unsigned long n = read_bytes;
      std::vector<char> output(n);
      auto status = limeReaderReadData(static_cast<void *>(output.data()), &n,
                                       lime_reader.get());

      if (status != LIME_SUCCESS)
        throw std::runtime_error{"Errors reading the SciDAC header XML"};

      XML_Node xml_node{std::string(output.data(), output.size()), "Chroma"};
      return parse_latt_size_string(
          xml_node["ProgramInfo"]["Setgeom"]["latt_size"].text());
    }
  }

  throw std::runtime_error{"No SciDAC XML header found in file \"" + filename +
                           "\""};
}

void fill_scidac_xml_header(QDP::XMLWriter &header)
{
  QDP::push(header, "Chroma");
  QDP::push(header, "ProgramInfo");
  QDP::push(header, "Setgeom");

  QDP::write(header, "latt_size", QDP::Layout::lattSize());
  QDP::write(header, "logical_size", QDP::Layout::logicalSize());
  QDP::write(header, "subgrid_size", QDP::Layout::subgridLattSize());
  QDP::write(header, "total_volume", QDP::Layout::vol());
  QDP::write(header, "subgrid_volume", QDP::Layout::sitesOnNode());

  QDP::pop(header);
  QDP::pop(header);
  QDP::pop(header);
}

void copy(su3_matrix const &from, QDP_Colour_Matrix &to, double prefactor)
{
  for (auto a = 0; a < 3; ++a) {
    for (auto b = 0; b < 3; ++b) {
      to.elem(b, a).real() = prefactor * from[a][b].real();
      to.elem(b, a).imag() = prefactor * from[a][b].imag();
    }
  }
}

void copy(propagator_matrix const &from, QDP_Propagator_Matrix &to)
{
  copy(from[3][3], to.elem(0, 0), 1.0);
  copy(from[2][3], to.elem(0, 1), -1.0);
  copy(from[1][3], to.elem(0, 2), -1.0);
  copy(from[0][3], to.elem(0, 3), 1.0);

  copy(from[3][2], to.elem(1, 0), -1.0);
  copy(from[2][2], to.elem(1, 1), 1.0);
  copy(from[1][2], to.elem(1, 2), 1.0);
  copy(from[0][2], to.elem(1, 3), -1.0);

  copy(from[3][1], to.elem(2, 0), -1.0);
  copy(from[2][1], to.elem(2, 1), 1.0);
  copy(from[1][1], to.elem(2, 2), 1.0);
  copy(from[0][1], to.elem(2, 3), -1.0);

  copy(from[3][0], to.elem(3, 0), 1.0);
  copy(from[2][0], to.elem(3, 1), -1.0);
  copy(from[1][0], to.elem(3, 2), -1.0);
  copy(from[0][0], to.elem(3, 3), 1.0);
}

void copy(QDP_Colour_Matrix const &from, su3_matrix &to, double prefactor)
{
  for (auto a = 0; a < 3; ++a) {
    for (auto b = 0; b < 3; ++b) {
      to[a][b] =
          prefactor * complex{from.elem(b, a).real(), from.elem(b, a).imag()};
    }
  }
}

void copy(QDP_Propagator_Matrix const &from, propagator_matrix &to)
{
  copy(from.elem(3, 3), to[0][0], 1.0);
  copy(from.elem(2, 3), to[0][1], -1.0);
  copy(from.elem(1, 3), to[0][2], -1.0);
  copy(from.elem(0, 3), to[0][3], 1.0);

  copy(from.elem(3, 2), to[1][0], -1.0);
  copy(from.elem(2, 2), to[1][1], 1.0);
  copy(from.elem(1, 2), to[1][2], 1.0);
  copy(from.elem(0, 2), to[1][3], -1.0);

  copy(from.elem(3, 1), to[2][0], -1.0);
  copy(from.elem(2, 1), to[2][1], 1.0);
  copy(from.elem(1, 1), to[2][2], 1.0);
  copy(from.elem(0, 1), to[2][3], -1.0);

  copy(from.elem(3, 0), to[3][0], 1.0);
  copy(from.elem(2, 0), to[3][1], 1.0);
  copy(from.elem(1, 0), to[3][2], 1.0);
  copy(from.elem(0, 0), to[3][3], 1.0);
}

} // namespace

class QDP_Propagator_IO::Propagator_Implementation
{
public:
  std::unique_ptr<QDP::LatticePropagator> propagator_ptr;
};

QDP_Propagator_IO::QDP_Propagator_IO()
    : propagator_impl_{make_unique<decltype(propagator_impl_)::element_type>()},
      qdp_instance_(QDP_Instance::get_instance()) {};

QDP_Propagator_IO::~QDP_Propagator_IO(){};

double QDP_Propagator_IO::square_norm() const
{
  auto sqr_f = [](double d) { return d * d; };

  double norm_sqr = 0.0;
  for (auto k = 0; k < QDP::Layout::vol(); ++k)
    for (auto i = 0; i < 4; ++i)
      for (auto j = 0; j < 4; ++j)
        for (auto a = 0; a < 3; ++a)
          for (auto b = 0; b < 3; ++b)
            norm_sqr += sqr_f(propagator_impl_->propagator_ptr->elem(k)
                                  .elem(i, j)
                                  .elem(a, b)
                                  .real()) +
                        sqr_f(propagator_impl_->propagator_ptr->elem(k)
                                  .elem(i, j)
                                  .elem(a, b)
                                  .imag());

  return norm_sqr;
}

QDP_Propagator_Reader::QDP_Propagator_Reader(){};

QDP_Propagator_Reader::~QDP_Propagator_Reader(){};

void QDP_Propagator_Reader::read_from_file(std::string const &filename,
                                           lattice_options &opt)
{
  auto parsed_size = parse_lattice_size_from_xml_header(filename);

  if (opt.lattice_size[0] == 0) {
    opt.lattice_size = parsed_size;
  } else if (opt.lattice_size != parsed_size) {
    throw std::runtime_error{"Propagator file lattice size different to the "
                             "one set as input parameter"};
  }

  QDP_Instance::set_lattice_size(opt);

  propagator_impl_->propagator_ptr = make_unique<QDP::LatticePropagator>();

  QDP::XMLReader lime_xml_header{};
  QDP::XMLReader lime_xml_record{};

  QDP::QDPFileReader lime_reader{lime_xml_header, filename, QDP::QDPIO_SERIAL};
  QDP::read(lime_reader, lime_xml_record, *(propagator_impl_->propagator_ptr));

  if (lime_reader.bad())
    throw std::runtime_error{"Errors reading input config file"};
}

void QDP_Propagator_Reader::get(lattice_site_iterator const &site_id,
                                propagator_matrix &output_matrix)
{
  static QDP::multi1d<int> site_index(4);

  site_index[0] = site_id.x();
  site_index[1] = site_id.y();
  site_index[2] = site_id.z();
  site_index[3] = site_id.t();

  copy(propagator_impl_->propagator_ptr->elem(
           QDP::Layout::linearSiteIndex(site_index)),
       output_matrix);
}

QDP_Propagator_Writer::QDP_Propagator_Writer(){};

QDP_Propagator_Writer::~QDP_Propagator_Writer(){};

void QDP_Propagator_Writer::initialise(lattice_options const &opt)
{
  QDP_Instance::set_lattice_size(opt);
  propagator_impl_->propagator_ptr = make_unique<QDP::LatticePropagator>();
}

void QDP_Propagator_Writer::write_to_file(std::string const &filename)
{
  if (!propagator_impl_ or !(propagator_impl_->propagator_ptr)) {
    throw std::runtime_error{"QDP_Propagator_Writer: Trying to write to "
                             "file without first initialising the writer"};
  }

  QDP::XMLBufferWriter header{};
  fill_scidac_xml_header(header);

  QDP::QDPFileWriter lime_writer{header, filename, QDP::QDPIO_SINGLEFILE,
                                 QDP::QDPIO_SERIAL, QDP::QDPIO_OPEN};

  if (lime_writer.bad())
    throw std::runtime_error{"Error in SciDAC outfile " + filename};

  QDP::XMLBufferWriter xml_record{};

  // Push an empty Params tag
  QDP::push(xml_record, "Params");
  QDP::pop(xml_record);

  QDP::write(lime_writer, xml_record, *(propagator_impl_->propagator_ptr));
  QDP::close(lime_writer);
}

void QDP_Propagator_Writer::put(lattice_site_iterator const &site_id,
                                propagator_matrix const &input_matrix)
{
  static QDP::multi1d<int> site_index(4);

  site_index[0] = site_id.x();
  site_index[1] = site_id.y();
  site_index[2] = site_id.z();
  site_index[3] = site_id.t();

  copy(input_matrix, propagator_impl_->propagator_ptr->elem(
                         QDP::Layout::linearSiteIndex(site_index)));
}

} // namespace oniconverter
} // namespace fastsum
