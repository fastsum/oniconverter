
/*
 * Created: 08-05-2018
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef CONFIG_IO_HPP
#define CONFIG_IO_HPP

#include "lib/types/lattice_options.hpp"
#include "lib/types/lattice_iterator.hpp"
#include "lib/types/su3_matrix.hpp"

namespace fastsum {
namespace oniconverter {

class Configuration_IO
{
public:
  virtual double plaquette() const = 0;

  virtual void initialise(lattice_options const &opt) = 0;
  virtual void finalise(lattice_options const &opt) = 0;

  virtual ~Configuration_IO(){};
};

class Configuration_Reader : public virtual Configuration_IO
{
public:
  virtual void read_from_file(std::string const &, lattice_options &opt) = 0;
  virtual void get(lattice_link_iterator const &, su3_matrix &) = 0;

  virtual ~Configuration_Reader(){};
};

class Configuration_Writer : public virtual Configuration_IO
{
public:
  virtual void write_to_file(std::string const &) = 0;
  virtual void put(lattice_link_iterator const &, su3_matrix const &) = 0;

  virtual ~Configuration_Writer(){};
};

} // namespace oniconverter
} // namespace fastsum

#endif /* CONFIG_IO_HPP */
