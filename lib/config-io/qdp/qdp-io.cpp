
/*
 * Created: 10-05-2018
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "qdp.h"

#include "lib/utility/make_unique.hpp"
#include "lib/utility/xml_parser.hpp"
#include "lib/utility/qdp/qdp-instance.hpp"
#include "qdp-io.hpp"

namespace fastsum {
namespace oniconverter {

namespace {

const std::string xml_lattice_size_tag = "latt_size";
const std::string scidac_file_header_tag = "scidac-file-xml";

using QDP_su3_matrix = QDP::PColorMatrix<QDP::RComplex<double>, 3>;

lattice_site_index parse_latt_size_string(std::string latt_size)
{
  std::istringstream iss{latt_size};
  lattice_site_index size;

  iss >> size[1] >> size[2] >> size[3] >> size[0];

  return size;
}

/* Parse the lattice size of a configuration from a QDP configuration file */
lattice_site_index parse_lattice_size_from_xml_header(std::string filename)
{
  auto ifs = std::unique_ptr<FILE, void (*)(FILE *)>{
      std::fopen(filename.c_str(), "r"), [](FILE *fptr) { std::fclose(fptr); }};

  auto lime_reader = std::unique_ptr<LimeReader, void (*)(LimeReader *)>{
      limeCreateReader(ifs.get()),
      [](LimeReader *reader) { limeDestroyReader(reader); }};

  while (limeReaderNextRecord(lime_reader.get()) == LIME_SUCCESS) {
    auto read_type = std::string{limeReaderType(lime_reader.get())};
    auto read_bytes = limeReaderBytes(lime_reader.get());

    if (read_type == scidac_file_header_tag) {
      unsigned long n = read_bytes;
      std::vector<char> output(n);
      auto status = limeReaderReadData(static_cast<void *>(output.data()), &n,
                                       lime_reader.get());

      if (status != LIME_SUCCESS)
        throw std::runtime_error{"Errors reading the SciDAC header XML"};

      XML_Node xml_node{std::string(output.data(), output.size()), "HMC"};
      return parse_latt_size_string(
          xml_node["ProgramInfo"]["Setgeom"]["latt_size"].text());
    }
  }

  throw std::runtime_error{"No SciDAC XML header found in file \"" + filename +
                           "\""};
}

void fill_scidac_xml_header(QDP::XMLWriter &header)
{
  QDP::push(header, "HMC");
  QDP::push(header, "ProgramInfo");
  QDP::push(header, "Setgeom");

  QDP::write(header, "latt_size", QDP::Layout::lattSize());
  QDP::write(header, "logical_size", QDP::Layout::logicalSize());
  QDP::write(header, "subgrid_size", QDP::Layout::subgridLattSize());
  QDP::write(header, "total_volume", QDP::Layout::vol());
  QDP::write(header, "subgrid_volume", QDP::Layout::sitesOnNode());

  QDP::pop(header);
  QDP::pop(header);
  QDP::pop(header);
}

void copy(QDP_su3_matrix const &source, su3_matrix &target)
{
  for (auto i = 0; i < 3; ++i) {
    for (auto j = 0; j < 3; ++j) {
      target[i][j] = {source.elem(i, j).real(), source.elem(i, j).imag()};
    }
  }
}

void copy(su3_matrix const &source, QDP_su3_matrix &target)
{
  for (auto i = 0; i < 3; ++i) {
    for (auto j = 0; j < 3; ++j) {
      target.elem(i, j).real() = source[i][j].real();
      target.elem(i, j).imag() = source[i][j].imag();
    }
  }
}

} // namespace

using QDP_Gauge_Field = QDP::multi1d<QDP::LatticeColorMatrixD>;

class QDP_Configuration_IO::Gauge_Implementation
{
public:
  std::unique_ptr<QDP_Gauge_Field> gauge_field_ptr;
};

QDP_Configuration_IO::QDP_Configuration_IO()
    : gauge_impl_{make_unique<decltype(gauge_impl_)::element_type>()},
      qdp_instance_(QDP_Instance::get_instance()) {};

QDP_Configuration_IO::~QDP_Configuration_IO(){};

double QDP_Configuration_IO::plaquette() const
{
  if (!gauge_impl_ or !gauge_impl_->gauge_field_ptr)
    throw std::runtime_error{
        "In QDP_Configuration_IO::plaquette: Trying to compute the plaquette "
        "before initialising the gauge field"};

  QDP::Double plaq_sum{0.};
  auto gfield = *(gauge_impl_->gauge_field_ptr);

  for (auto mu = 0; mu < 4; ++mu)
    for (auto nu = 0; nu < mu; ++nu)
      plaq_sum += QDP::sum(QDP::real(
          QDP::trace(gfield[mu] * QDP::shift(gfield[nu], FORWARD, mu) *
                     QDP::adj(QDP::shift(gfield[mu], FORWARD, nu)) *
                     QDP::adj(gfield[nu]))));

  plaq_sum *= QDP::Double(1. / (QDP::Layout::vol() * 18.));

  return QDP::toDouble(plaq_sum);
}

QDP_Configuration_Reader::QDP_Configuration_Reader(){};

QDP_Configuration_Reader::~QDP_Configuration_Reader(){};

void QDP_Configuration_Reader::read_from_file(std::string const &filename,
                                              lattice_options &opt)
{
  auto parsed_size = parse_lattice_size_from_xml_header(filename);

  if (opt.lattice_size[0] == 0) {
    opt.lattice_size = parsed_size;
  } else if (opt.lattice_size != parsed_size) {
    throw std::runtime_error{"Configuration file lattice size different to the "
                             "one set as input parameter"};
  }

  QDP_Instance::set_lattice_size(opt);

  gauge_impl_->gauge_field_ptr = make_unique<QDP_Gauge_Field>(4);

  QDP::XMLReader lime_xml_header{};
  QDP::XMLReader lime_xml_record{};

  QDP::QDPFileReader lime_reader{lime_xml_header, filename, QDP::QDPIO_SERIAL};
  QDP::read(lime_reader, lime_xml_record, *(gauge_impl_->gauge_field_ptr));

  if (lime_reader.bad())
    throw std::runtime_error{"Errors reading input config file"};
}

void QDP_Configuration_Reader::get(lattice_link_iterator const &link_id,
                                   su3_matrix &output_matrix)
{
  static QDP::multi1d<int> site_index(4);
  auto qdp_mu = (link_id.mu() + 3) % 4;

  site_index[0] = link_id.x();
  site_index[1] = link_id.y();
  site_index[2] = link_id.z();
  site_index[3] = link_id.t();

  copy((*(gauge_impl_->gauge_field_ptr))[qdp_mu]
           .elem(QDP::Layout::linearSiteIndex(site_index))
           .elem(),
       output_matrix);
}

QDP_Configuration_Writer::QDP_Configuration_Writer(){};

QDP_Configuration_Writer::~QDP_Configuration_Writer(){};

void QDP_Configuration_Writer::initialise(lattice_options const &opt)
{
  QDP_Instance::set_lattice_size(opt);
  gauge_impl_->gauge_field_ptr = make_unique<QDP_Gauge_Field>(4);
}

void QDP_Configuration_Writer::write_to_file(std::string const &filename)
{
  if (!gauge_impl_ or !(gauge_impl_->gauge_field_ptr)) {
    throw std::runtime_error{"QDP_Configuration_Writer: Trying to write to "
                             "file without first initialising the writer"};
  }

  QDP::XMLBufferWriter header{};
  fill_scidac_xml_header(header);

  QDP::QDPFileWriter lime_writer{header, filename, QDP::QDPIO_SINGLEFILE,
                                 QDP::QDPIO_SERIAL, QDP::QDPIO_OPEN};

  if (lime_writer.bad())
    throw std::runtime_error{"Error in SciDAC outfile " + filename};

  QDP::XMLBufferWriter xml_record{};

  // Push an empty Params tag
  QDP::push(xml_record, "Params");
  QDP::pop(xml_record);

  QDP::write(lime_writer, xml_record, *(gauge_impl_->gauge_field_ptr));
  QDP::close(lime_writer);
}

void QDP_Configuration_Writer::put(lattice_link_iterator const &link_id,
                                   su3_matrix const &input_matrix)
{
  static QDP::multi1d<int> site_index(4);
  auto qdp_mu = (link_id.mu() + 3) % 4;

  site_index[0] = link_id.x();
  site_index[1] = link_id.y();
  site_index[2] = link_id.z();
  site_index[3] = link_id.t();

  copy(input_matrix, (*(gauge_impl_->gauge_field_ptr))[qdp_mu]
                         .elem(QDP::Layout::linearSiteIndex(site_index))
                         .elem());
}

} // namespace oniconverter
} // namespace fastsum
