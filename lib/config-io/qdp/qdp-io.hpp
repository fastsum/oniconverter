
/*
 * Created: 10-05-2018
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef QDP_IO_HPP
#define QDP_IO_HPP

#include "lib/config-io/config_io.hpp"
#include <memory>

namespace fastsum {
namespace oniconverter {

class QDP_Instance;

class QDP_Configuration_IO : public virtual Configuration_IO
{
public:
  QDP_Configuration_IO();

  double plaquette() const override;
  void finalise(lattice_options const &) override{};

  virtual ~QDP_Configuration_IO();

protected:
  class Gauge_Implementation;
  std::unique_ptr<Gauge_Implementation> gauge_impl_;

private:
  QDP_Instance& qdp_instance_;
};

class QDP_Configuration_Reader : public Configuration_Reader,
                                 public QDP_Configuration_IO
{
public:
  QDP_Configuration_Reader();

  void initialise(lattice_options const &) override{};

  void read_from_file(std::string const &, lattice_options &opt) override;
  void get(lattice_link_iterator const &link_id,
           su3_matrix &output_matrix) override;

  virtual ~QDP_Configuration_Reader();
};

class QDP_Configuration_Writer : public Configuration_Writer,
                                 public QDP_Configuration_IO
{
public:
  QDP_Configuration_Writer();

  void initialise(lattice_options const &opt) override;

  void write_to_file(std::string const &) override;
  void put(lattice_link_iterator const &link_id,
           su3_matrix const &input_matrix) override;

  virtual ~QDP_Configuration_Writer();
};

} // namespace oniconverter
} // namespace fastsum

#endif /* QDP_IO_HPP */
