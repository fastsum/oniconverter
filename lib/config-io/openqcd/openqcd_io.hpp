
/*
 * Created: 08-05-2018
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef OPENQCD_IO_HPP
#define OPENQCD_IO_HPP

#include "lib/config-io/config_io.hpp"

namespace fastsum {
namespace oniconverter {

class openQCD_Configuration_IO : public virtual Configuration_IO
{
public:
  double plaquette() const override;

  void finalise(lattice_options const &) override{};

  virtual ~openQCD_Configuration_IO(){};
};

class openQCD_Configuration_Reader : public Configuration_Reader,
                                     public openQCD_Configuration_IO
{
public:
  void initialise(lattice_options const &) override{};

  void read_from_file(std::string const &, lattice_options &opt) override;
  void get(lattice_link_iterator const &link_id, su3_matrix &output_matrix) override;

  virtual ~openQCD_Configuration_Reader(){};
};

class openQCD_Configuration_Writer : public Configuration_Writer,
                                     public openQCD_Configuration_IO
{
public:
  void initialise(lattice_options const &opt) override;

  void write_to_file(std::string const &) override;
  void put(lattice_link_iterator const &link_id,
           su3_matrix const &input_matrix) override;

  virtual ~openQCD_Configuration_Writer(){};
};

} // namespace oniconverter
} // namespace fastsum

#endif /* OPENQCD_IO_HPP */
