
/*
 * Created: 08-05-2018
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "openqcd_io.hpp"
#include "lib/utility/openqcd/instance.hpp"
#include <cstdio>
#include <openqcd.hpp>

namespace fastsum {
namespace oniconverter {

namespace {

/* Parse the lattice size of a configuration from an openQCD configuration file
 * */
lattice_site_index parse_lattice_size_from_infile(std::string const &filename)
{
  std::array<int, 4> parsing_array;
  std::FILE *ifs;

  ifs = std::fopen(filename.c_str(), "rb");

  if (ifs == nullptr)
    throw std::runtime_error{"Unable to open file \"" + filename +
                             "\" for reading."};

  std::fread(parsing_array.data(), sizeof(int), 4, ifs);
  std::fclose(ifs);

  return {static_cast<std::size_t>(parsing_array[0]),
          static_cast<std::size_t>(parsing_array[1]),
          static_cast<std::size_t>(parsing_array[2]),
          static_cast<std::size_t>(parsing_array[3])};
}

/* The superindex of pos (t,x,y,z) for the OpenQCD storage */
int openqcd_site_index(int t, int x, int y, int z)
{
  return openqcd::ipt()[z + openqcd::L3() *
                                (y + openqcd::L2() * (x + openqcd::L1() * t))];
}

/* Checks if a site is odd or even */
bool is_odd_site(int t, int x, int y, int z)
{
  return ((t + x + y + z) % 2 == 1);
}

/* Returns the index of the gauge link at pos (t,x,y,z) in dir mu in the openqcd
 * storage format. It assumes that the program runs in serial and that all
 * openqcd geometry structures exist. */
int openqcd_gauge_index(int t, int x, int y, int z, int mu)
{
  auto site_index = openqcd_site_index(t, x, y, z);

  if (is_odd_site(t, x, y, z))
    return 8 * (site_index - openqcd::VOLUME() / 2) + 2 * mu;
  else
    return 8 * (openqcd::iup()[site_index][mu] - openqcd::VOLUME() / 2) +
           2 * mu + 1;
}

void copy(openqcd::complex_dble const &source, complex &target)
{
  target = {source.re, source.im};
}

void copy(openqcd::su3_dble const &source, su3_matrix &target)
{
  copy(source.c11, target[0][0]);
  copy(source.c12, target[0][1]);
  copy(source.c13, target[0][2]);

  copy(source.c21, target[1][0]);
  copy(source.c22, target[1][1]);
  copy(source.c23, target[1][2]);

  copy(source.c31, target[2][0]);
  copy(source.c32, target[2][1]);
  copy(source.c33, target[2][2]);
}

void copy(complex const &source, openqcd::complex_dble &target)
{
  target.re = source.real();
  target.im = source.imag();
}

void copy(su3_matrix const &source, openqcd::su3_dble &target)
{
  copy(source[0][0], target.c11);
  copy(source[0][1], target.c12);
  copy(source[0][2], target.c13);

  copy(source[1][0], target.c21);
  copy(source[1][1], target.c22);
  copy(source[1][2], target.c23);

  copy(source[2][0], target.c31);
  copy(source[2][1], target.c32);
  copy(source[2][2], target.c33);
}

} // namespace

double openQCD_Configuration_IO::plaquette() const
{
  static const double normalisation = 18. * openqcd::VOLUME();

  if (!openQCD_Instance::is_geometry_set())
    throw std::runtime_error{
        "Cannot measure the plaquette before OpenQCD is initialised"};

  return openqcd::uflds::plaq_wsum_dble(0) / normalisation;
}

void openQCD_Configuration_Reader::get(lattice_link_iterator const &link_id,
                                       su3_matrix &output_matrix)
{
  copy(openqcd::uflds::udfld()[openqcd_gauge_index(
           link_id.t(), link_id.x(), link_id.y(), link_id.z(), link_id.mu())],
       output_matrix);
}

void openQCD_Configuration_Reader::read_from_file(std::string const &filename,
                                                  lattice_options &opt)
{
  auto parsed_size = parse_lattice_size_from_infile(filename);

  if (opt.lattice_size[0] == 0) {
    opt.lattice_size = parsed_size;
  } else if (opt.lattice_size != parsed_size) {
    throw std::runtime_error{"Configuration file lattice size different to the "
                             "one set as input parameter"};
  }

  openQCD_Instance::set_lattice_size(opt);
  openqcd::archive::import_cnfg(filename.c_str());

  if (!openqcd::lattice::check_bc(64.0 * DBL_EPSILON) or
      openqcd::flags::query_flags(UD_IS_CLEAN) == 0)
    throw std::runtime_error{"Unexpeced boundary values for OpenQCD config"};
}

void openQCD_Configuration_Writer::initialise(lattice_options const &opt)
{
  openQCD_Instance::set_lattice_size(opt);
}

void openQCD_Configuration_Writer::put(lattice_link_iterator const &link_id,
                                       su3_matrix const &input_matrix)
{
  copy(input_matrix,
       openqcd::uflds::udfld()[openqcd_gauge_index(
           link_id.t(), link_id.x(), link_id.y(), link_id.z(), link_id.mu())]);
}

void openQCD_Configuration_Writer::write_to_file(std::string const &filename)
{
  if (!openqcd::lattice::check_bc(64.0 * DBL_EPSILON) or
      openqcd::flags::query_flags(UD_IS_CLEAN) == 0)
    throw std::runtime_error{"Unexpeced boundary values for OpenQCD config"};

  openqcd::archive::export_cnfg(filename.c_str());
}

} // namespace oniconverter
} // namespace fastsum
