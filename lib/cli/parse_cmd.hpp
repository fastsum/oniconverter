
/*
 * Created: 16-05-2018
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef PARSE_CMD_HPP
#define PARSE_CMD_HPP

#include <string>
#include "lib/types/lattice_params.hpp"

namespace fastsum {
namespace oniconverter {

struct Config_IO_Identifier
{
  std::string type_string, filename;
};

struct Program_Arguments
{
  Config_IO_Identifier reader, writer;
  lattice_site_index lattice_size;
};

Program_Arguments parse_program_arguments(int argc, char **argv);

} // namespace oniconverter 
} // namespace fastsum 

#endif /* PARSE_CMD_HPP */
