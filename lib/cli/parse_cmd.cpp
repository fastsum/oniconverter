
/*
 * Created: 16-05-2018
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "parse_cmd.hpp"
#include "lib/builders/builder_utilities.hpp"
#include "tclap/CmdLine.h"

namespace fastsum {
namespace oniconverter {

void parse_lattice_size(std::string const &size_str, Program_Arguments &args)
{
  std::string buffer;
  std::istringstream iss{size_str};

  std::vector<std::size_t> lattice_array;

  while (std::getline(iss, buffer, 'x'))
    lattice_array.push_back(std::stol(buffer));

  if (lattice_array.size() == 2) {
    args.lattice_size[0] = lattice_array[0];
    for (auto i = 1; i < 4; ++i)
      args.lattice_size[i] = lattice_array[1];
  } else if (lattice_array.size() == 4) {
    for (auto i = 0; i < 4; ++i)
      args.lattice_size[i] = lattice_array[i];
  } else {
    throw std::runtime_error{"Parse error: invalid lattice size argument"};
  }
}

Program_Arguments parse_program_arguments(int argc, char **argv)
{
  Program_Arguments parsed_parms;
  parsed_parms.lattice_size = {0, 0, 0, 0};

  TCLAP::CmdLine cmd("", ' ', "1.0");

  TCLAP::ValueArg<std::string> lattice_size_flag{
      "",
      "size",
      "Lattice size, formatted either as nt,nx,ny,nz or as nt,ns where ns is "
      "all spatial directions",
      false,
      "",
      "string"};

  cmd.add(lattice_size_flag);

  auto valid_types_strs = valid_filetypes();
  TCLAP::ValuesConstraint<std::string> filetype_constraint(valid_types_strs);

  TCLAP::UnlabeledValueArg<std::string> read_type_flag{
      "input_filetype", "The input filetype", true, "", &filetype_constraint};

  cmd.add(read_type_flag);

  TCLAP::UnlabeledValueArg<std::string> read_filename_flag{
      "input_filename", "The input filename", true, "", "filename"};

  cmd.add(read_filename_flag);

  TCLAP::UnlabeledValueArg<std::string> write_type_flag{
      "output_filetype", "The output filetype", true, "", &filetype_constraint};

  cmd.add(write_type_flag);

  TCLAP::UnlabeledValueArg<std::string> write_filename_flag{
      "output_filename", "The output filename", true, "", "filename"};

  cmd.add(write_filename_flag);

  cmd.parse(argc, argv);

  parsed_parms.reader.type_string = read_type_flag.getValue();
  parsed_parms.reader.filename = read_filename_flag.getValue();

  parsed_parms.writer.type_string = write_type_flag.getValue();
  parsed_parms.writer.filename = write_filename_flag.getValue();

  if (lattice_size_flag.isSet()) {
    parse_lattice_size(lattice_size_flag.getValue(), parsed_parms);
  }

  return parsed_parms;
}

} // namespace oniconverter
} // namespace fastsum
