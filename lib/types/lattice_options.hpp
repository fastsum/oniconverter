
/*
 * Created: 09-05-2018
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef LATTICE_OPTIONS_HPP
#define LATTICE_OPTIONS_HPP

#include "lattice_params.hpp"

namespace fastsum {
namespace oniconverter {

struct lattice_options
{
  lattice_options() : lattice_size{0, 0, 0, 0} {};

  lattice_site_index lattice_size;
};

} // namespace oniconverter
} // namespace fastsum

#endif /* LATTICE_OPTIONS_HPP */
