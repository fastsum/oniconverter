
/*
 * Created: 08-05-2018
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef LATTICE_PARAMS_HPP
#define LATTICE_PARAMS_HPP

#include <array>

namespace fastsum {
namespace oniconverter {

using lattice_site_index = std::array<std::size_t, 4>;
using lattice_link_index = std::array<std::size_t, 5>;

} // namespace oniconverter
} // namespace fastsum

#endif /* LATTICE_PARAMS_HPP */
