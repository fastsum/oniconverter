
/*
 * Created: 08-05-2018
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef SU3_MATRIX_HPP
#define SU3_MATRIX_HPP

#include <array>
#include <complex>

namespace fastsum {
namespace oniconverter {

using complex = std::complex<double>;
using su3_vector = std::array<complex, 3>;
using su3_matrix = std::array<su3_vector, 3>;

} // namespace oniconverter
} // namespace fastsum

#endif /* SU3_MATRIX_HPP */
