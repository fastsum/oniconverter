
/*
 * Created: 21-05-2018
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef PROPAGATOR_MATRIX_HPP
#define PROPAGATOR_MATRIX_HPP

#include "su3_matrix.hpp"

namespace fastsum {
namespace oniconverter {

using propagator_matrix = std::array<std::array<su3_matrix, 4>, 4>;

} // namespace oniconverter
} // namespace fastsum

#endif /* PROPAGATOR_MATRIX_HPP */
