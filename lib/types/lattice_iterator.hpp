
/*
 * Created: 08-05-2018
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef LATTICE_ITERATOR_HPP
#define LATTICE_ITERATOR_HPP

#include "lattice_params.hpp"

namespace fastsum {
namespace oniconverter {

template <std::size_t N>
class flat_iterator
{
public:
  using array_type = std::array<std::size_t, N>;
  using index_type = typename array_type::value_type;

  explicit flat_iterator(array_type const &size) : size_{size} { reset(); }

  flat_iterator(flat_iterator const &) = default;
  flat_iterator(flat_iterator &&) = default;

  flat_iterator &operator=(flat_iterator const &) = default;
  flat_iterator &operator=(flat_iterator &&) = default;

  index_type const operator[](std::size_t idx) const noexcept
  {
    return index_[idx];
  }

  flat_iterator const &operator++() noexcept
  {
    for (auto dir = size_.size() - 1; dir >= 0; --dir) {
      if (index_[dir] < size_[dir] - 1) {
        ++index_[dir];
        break;
      } else if (dir == 0) {
        index_[dir] = size_[dir];
        break;
      }
      index_[dir] = 0;
    }

    return *this;
  }

  flat_iterator operator++(int) noexcept
  {
    auto result = *this;
    this->operator++();
    return result;
  }

  flat_iterator const end() const noexcept
  {
    auto end_ = flat_iterator{size_};
    end_.index_[0] = size_[0];
    return end_;
  }

  bool operator==(flat_iterator const &rhs) const noexcept
  {
    return this->size_ == rhs.size_ and this->index_ == rhs.index_;
  }

  bool done() const noexcept { return index_[0] == size_[0]; }

  void reset() noexcept
  {
    for (auto &i : index_)
      i = 0;
  }

  virtual ~flat_iterator(){};

private:
  array_type const size_;
  array_type index_;
};

// A flag lattice link iterator
class lattice_link_iterator : public flat_iterator<5>
{
public:
  explicit lattice_link_iterator(lattice_link_index const &size)
      : flat_iterator{size} {};

  explicit lattice_link_iterator(lattice_site_index const &size)
      : lattice_link_iterator{{size[0], size[1], size[2], size[3], 4}} {};

  lattice_link_iterator(lattice_link_iterator const &) = default;
  lattice_link_iterator(lattice_link_iterator &&) = default;

  index_type const t() const noexcept { return this->operator[](0); }

  index_type const x() const noexcept { return this->operator[](1); }

  index_type const y() const noexcept { return this->operator[](2); }

  index_type const z() const noexcept { return this->operator[](3); }

  index_type const mu() const noexcept { return this->operator[](4); }
};

// A flag lattice link iterator
class lattice_site_iterator : public flat_iterator<4>
{
public:
  explicit lattice_site_iterator(lattice_site_index const &size)
      : flat_iterator{size} {};

  lattice_site_iterator(lattice_site_iterator const &) = default;
  lattice_site_iterator(lattice_site_iterator &&) = default;

  index_type const t() const noexcept { return this->operator[](0); }

  index_type const x() const noexcept { return this->operator[](1); }

  index_type const y() const noexcept { return this->operator[](2); }

  index_type const z() const noexcept { return this->operator[](3); }
};

} // namespace oniconverter
} // namespace fastsum

#endif /* LATTICE_ITERATOR_HPP */
