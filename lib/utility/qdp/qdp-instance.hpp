
/*
 * Created: 10-05-2018
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef QDP_INSTANCE_HPP
#define QDP_INSTANCE_HPP

#include "lib/types/lattice_options.hpp"
#include "lib/utility/make_unique.hpp"
#include <qdp.h>

namespace fastsum {
namespace oniconverter {

class Mute_Cout
{
public:
  Mute_Cout() : Mute_Cout{"/dev/null"} {};

  Mute_Cout(std::string redirect_filename) : redirect_stream{redirect_filename}
  {
    if (!redirect_stream) {
      throw std::runtime_error{
          "In Mute_Cout constructor: Unable to redirect to file \"" +
          redirect_filename + "\""};
    }

    original_stream = std::cout.rdbuf();
    std::cout.rdbuf(redirect_stream.rdbuf());
  }

  ~Mute_Cout()
  {
    std::cout.rdbuf(original_stream);
    redirect_stream.close();
  }

private:
  std::ofstream redirect_stream;
  std::streambuf *original_stream;
};

class QDP_Stream_Redirect
{
public:
  QDP_Stream_Redirect() : QDP_Stream_Redirect{"/dev/null"} {};

  QDP_Stream_Redirect(std::string new_cout_filename)
      : QDP_Stream_Redirect{new_cout_filename, new_cout_filename} {};

  QDP_Stream_Redirect(std::string new_cout_filename,
                      std::string new_cerr_filename)
      : cout_ofs{new_cout_filename}, cerr_ofs{new_cerr_filename}
  {
    if (!cout_ofs)
      throw std::runtime_error{"Cannot open \"" + new_cout_filename +
                               "\" for writing"};

    if (!cerr_ofs)
      throw std::runtime_error{"Cannot open \"" + new_cerr_filename +
                               "\" for writing"};

    QDP::QDPIO::cout.init(&cout_ofs);
    QDP::QDPIO::cerr.init(&cerr_ofs);
  }

  ~QDP_Stream_Redirect()
  {
    QDP::QDPIO::cout.init(&std::cout);
    QDP::QDPIO::cerr.init(&std::cerr);
  }

private:
  std::ofstream cout_ofs;
  std::ofstream cerr_ofs;
};

class QDP_Instance
{
public:
  static QDP_Instance& get_instance()
  {
    static QDP_Instance instance_;
    return instance_;
  }

  QDP_Instance(QDP_Instance const&) = delete;
  void operator=(QDP_Instance const&) = delete;

private:
  QDP_Instance()
  {
    int args = 0;
    char *argv[] = {const_cast<char *>("qdp-instance")};
    char **argv_ptr = argv;

    {
      Mute_Cout cout_lock{};
      QDP::QDP_initialize(&args, &argv_ptr);
    }

    qdp_redirect_ = make_unique<decltype(qdp_redirect_)::element_type>();
  }

  ~QDP_Instance() { QDP::QDP_finalize(); }

public:
  static void set_lattice_size(lattice_options const &opt)
  {
    if (is_lattice_sizes_set()) {
      if (!is_same_lattice_sizes(opt))
        throw std::runtime_error{
            "Trying to reset the openQCD lattice size to a new size"};
    } else {
      if (!is_initialized()) {
        throw std::runtime_error{"Cannot set the lattice size of QDP without "
                                 "initialising the framework first"};
      }

      QDP::multi1d<int> lsize(4);

      lsize[0] = static_cast<int>(opt.lattice_size[1]);
      lsize[1] = static_cast<int>(opt.lattice_size[2]);
      lsize[2] = static_cast<int>(opt.lattice_size[3]);
      lsize[3] = static_cast<int>(opt.lattice_size[0]);

      QDP::Layout::setLattSize(lsize);
      QDP::Layout::create();
    }
  }

  static lattice_site_index lattice_size()
  {
    auto lsize = QDP::Layout::lattSize();

    return {
        static_cast<std::size_t>(lsize[3]), static_cast<std::size_t>(lsize[0]),
        static_cast<std::size_t>(lsize[1]), static_cast<std::size_t>(lsize[2])};
  }

  static bool is_same_lattice_sizes(lattice_options const &opt)
  {
    if (!is_lattice_sizes_set()) {
      return false;
    }

    return opt.lattice_size == lattice_size();
  }

  static bool is_lattice_sizes_set() { return QDP::Layout::vol() != 0; }

  static bool is_initialized() { return QDP::QDP_isInitialized(); }

private:
  std::unique_ptr<QDP_Stream_Redirect> qdp_redirect_;
};

} // namespace oniconverter
} // namespace fastsum

#endif /* QDP_INSTANCE_HPP */
