
/*
 * Created: 08-05-2018
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef OPENQCD_INSTANCE_HPP
#define OPENQCD_INSTANCE_HPP

#include "openqcd.hpp"
#include <algorithm>

#include "lib/types/lattice_options.hpp"
#include "lib/utility/mpi_instance.hpp"

namespace fastsum {
namespace oniconverter {

class openQCD_Instance
{
public:
  static void set_lattice_size(lattice_options const &opt)
  {
    if (is_lattice_sizes_set()) {
      if (!is_same_lattice_sizes(opt))
        throw std::runtime_error{
            "Trying to reset the openQCD lattice size to a new size"};
    } else {
      int nproc[4] = {1, 1, 1, 1};
      int lsize[4] = {static_cast<int>(opt.lattice_size[0]),
                      static_cast<int>(opt.lattice_size[1]),
                      static_cast<int>(opt.lattice_size[2]),
                      static_cast<int>(opt.lattice_size[3])};

      openqcd::set_lattice_sizes(nproc, lsize, nproc);

      double theta[3] = {0.0};
      openqcd::flags::set_bc_parms(3, 0., 0., 0., 0., nullptr, nullptr, theta);

      set_geometry();
    }
  }

  static void set_geometry() noexcept
  {
    if (!is_geometry_set()) {
      openqcd::lattice::geometry();
    }
  }

  static lattice_site_index lattice_size() noexcept
  {
    return {static_cast<std::size_t>(openqcd::L0()),
            static_cast<std::size_t>(openqcd::L1()),
            static_cast<std::size_t>(openqcd::L2()),
            static_cast<std::size_t>(openqcd::L3())};
  }

  static bool is_same_lattice_sizes(lattice_options const &opt) noexcept
  {
    if (!is_lattice_sizes_set()) {
      return false;
    }

    return opt.lattice_size == lattice_size();
  }

  static bool is_lattice_sizes_set() noexcept
  {
    return openqcd::iup() != nullptr;
  }

  static bool is_geometry_set() noexcept
  {
    return is_lattice_sizes_set() and (openqcd::iup()[0][0] != 0);
  }
};

} // namespace oniconverter
} // namespace fastsum

#endif /* OPENQCD_INSTANCE_HPP */
