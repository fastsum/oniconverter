
/*
 * Created: 10-05-2018
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef MAKE_UNIQUE_HPP
#define MAKE_UNIQUE_HPP

#include <memory>

namespace fastsum {
namespace oniconverter {

template <typename T, typename... Args>
std::unique_ptr<T> make_unique(Args &&... args)
{
  return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}

template <typename T>
std::unique_ptr<T> new_unique(T *raw_ptr)
{
  return {raw_ptr};
}

} // namespace oniconverter
} // namespace fastsum

#endif /* MAKE_UNIQUE_HPP */
