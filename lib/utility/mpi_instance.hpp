
/*
 * Created: 08-05-2018
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef MPI_INSTANCE_HPP
#define MPI_INSTANCE_HPP

#include <mpi.h>

namespace fastsum {
namespace oniconverter {

class MPI_Instance
{
public:
  MPI_Instance(int argc, char **argv) { MPI_Init(&argc, &argv); }

  ~MPI_Instance()
  {
    if (!is_finalized()) {
      MPI_Finalize();
    }
  }

  static int comm_size() noexcept
  {
    int comm_size;
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    return comm_size;
  }

  static int my_rank() noexcept
  {
    int my_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    return my_rank;
  }

  static bool is_initialized() noexcept
  {
    int initialized_;
    MPI_Initialized(&initialized_);
    return static_cast<bool>(initialized_);
  }

  static bool is_finalized() noexcept
  {
    int finalized_;
    MPI_Finalized(&finalized_);
    return static_cast<bool>(finalized_);
  }
};

} // namespace oniconverter
} // namespace fastsum

#endif /* MPI_INSTANCE_HPP */
