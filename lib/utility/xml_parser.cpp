
/*
 * Created: 11-05-2018
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "xml_parser.hpp"
#include "lib/utility/make_unique.hpp"
#include "tinyxml2.h"
#include <stdexcept>

namespace fastsum {
namespace oniconverter {

XML_Node::XML_Node(XML_Node const &rhs) : node_{rhs.node_} {}

XML_Node::XML_Node(XML_Node::node_type *node) : node_{node}
{
  if (node == nullptr)
    throw std::runtime_error{
        "Initialising XML_Node with an invalid initial node"};
}

XML_Node::XML_Node(std::string const &xml_string, std::string const &first_node)
{
  doc_ = make_unique<XML_Node::document_type>();

  if (doc_->Parse(xml_string.c_str()))
    throw std::runtime_error{
        "XML_Node: There was an error parsing the xml string"};

  node_ = doc_->FirstChildElement(first_node.c_str());

  if (!node_)
    throw std::runtime_error{
        "XML_Node: The XML document does not contain the key \"" + first_node +
        "\""};
}

XML_Node::~XML_Node(){};

XML_Node XML_Node::operator[](std::string key)
{
  auto child = node_->FirstChildElement(key.c_str());

  if (child == nullptr) {
    throw std::runtime_error{"XML_Node: \"" + key +
                             "\" is not a valid element of tag \"" +
                             node_->Name() + "\""};
  }

  return XML_Node{child};
}

std::string XML_Node::text() const
{
  auto text_value = node_->GetText();

  if (text_value == nullptr) {
    throw std::runtime_error{std::string{"XML_Node: \""} + node_->Name() +
                             "\" does not have a text field"};
  }

  return {text_value};
}

} // namespace oniconverter
} // namespace fastsum
