
/*
 * Created: 11-05-2018
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef XML_PARSER_HPP
#define XML_PARSER_HPP

#include <memory>
#include <string>

namespace tinyxml2 {
class XMLDocument;
class XMLElement;
} // namespace tinyxml2

namespace fastsum {
namespace oniconverter {

class XML_Node
{
public:
  using node_type = tinyxml2::XMLElement;

  XML_Node(XML_Node const &rhs);

  XML_Node(node_type *node);
  XML_Node(std::string const &xml_string, std::string const &first_node);

  XML_Node operator[](std::string key);
  std::string text() const;

  ~XML_Node();

private:
  using document_type = tinyxml2::XMLDocument;

  node_type *node_;
  std::unique_ptr<document_type> doc_;
};

} // namespace oniconverter
} // namespace fastsum

#endif /* XML_PARSER_HPP */
