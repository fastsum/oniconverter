
/*
 * Created: 26-05-2018
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "propagator_io_builder.hpp"
#include "lib/propagator-io/openqcd/openqcd_io.hpp"
#include "lib/propagator-io/qdp/qdp-io.hpp"
#include "lib/utility/make_unique.hpp"

namespace fastsum {
namespace oniconverter {

std::unique_ptr<Propagator_Reader> create_reader(Filetypes ft)
{
  switch (ft) {
  case Filetypes::openQCD:
    return make_unique<openQCD_Propagator_Reader>();
  case Filetypes::QDP:
    return make_unique<QDP_Propagator_Reader>();
  default:
    throw std::runtime_error{"create_reader: Invalid filetype"};
  }
}

std::unique_ptr<Propagator_Writer> create_writer(Filetypes ft)
{
  switch (ft) {
  case Filetypes::openQCD:
    return make_unique<openQCD_Propagator_Writer>();
  case Filetypes::QDP:
    return make_unique<QDP_Propagator_Writer>();
  default:
    throw std::runtime_error{"create_writer: Invalid filetype"};
  }
}

} // namespace oniconverter
} // namespace fastsum
