
/*
 * Created: 17-05-2018
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "config_io_builder.hpp"
#include "lib/config-io/openqcd/openqcd_io.hpp"
#include "lib/config-io/qdp/qdp-io.hpp"
#include "lib/utility/make_unique.hpp"

namespace fastsum {
namespace oniconverter {

std::unique_ptr<Configuration_Reader> create_reader(Filetypes ft)
{
  switch (ft) {
  case Filetypes::openQCD:
    return make_unique<openQCD_Configuration_Reader>();
  case Filetypes::QDP:
    return make_unique<QDP_Configuration_Reader>();
  default:
    throw std::runtime_error{"create_reader: Invalid filetype"};
  }
}

std::unique_ptr<Configuration_Writer> create_writer(Filetypes ft)
{
  switch (ft) {
  case Filetypes::openQCD:
    return make_unique<openQCD_Configuration_Writer>();
  case Filetypes::QDP:
    return make_unique<QDP_Configuration_Writer>();
  default:
    throw std::runtime_error{"create_writer: Invalid filetype"};
  }
}

} // namespace oniconverter
} // namespace fastsum
