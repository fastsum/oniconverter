
/*
 * Created: 26-05-2018
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef PROPAGATOR_IO_BUILDER_HPP
#define PROPAGATOR_IO_BUILDER_HPP

#include "builder_utilities.hpp"
#include "lib/propagator-io/propagator_io.hpp"
#include <memory>
#include <vector>

namespace fastsum {
namespace oniconverter {

std::unique_ptr<Propagator_Reader> create_reader(Filetypes ft);
std::unique_ptr<Propagator_Writer> create_writer(Filetypes ft);

} // namespace oniconverter
} // namespace fastsum

#endif /* PROPAGATOR_IO_BUILDER_HPP */
