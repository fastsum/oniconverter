
/*
 * Created: 26-05-2018
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include "builder_utilities.hpp"
#include <stdexcept>

namespace fastsum {
namespace oniconverter {

namespace {

template <typename Enum>
constexpr std::size_t enum_value(Enum e)
{
  return static_cast<std::size_t>(e);
}

template <typename Enum>
constexpr std::size_t enum_size()
{
  return static_cast<std::size_t>(Enum::End_);
}

template <typename Enum>
std::vector<std::string> list_enums()
{
  std::vector<std::string> enum_names;

  for (auto i = enum_value(Enum::Begin_) + 1; i != enum_value(Enum::End_); ++i)
    enum_names.push_back(to_string(static_cast<Enum>(i)));

  return enum_names;
}

template <typename Enum>
Enum string_to_enum(std::string const &str)
{
  for (auto i = enum_value(Enum::Begin_) + 1; i != enum_value(Enum::End_); ++i)
    if (to_string(static_cast<Enum>(i)) == str)
      return static_cast<Enum>(i);

  return Enum::End_;
}

} // namespace

Filetypes to_filetype(std::string ft_str)
{
  auto ft = string_to_enum<Filetypes>(ft_str);

  if (ft == Filetypes::End_) {
    throw std::runtime_error{
        "to_filetype(string): Invalid string to convert to filetype"};
  }

  return ft;
}

std::string to_string(Filetypes ft)
{
  switch (ft) {
  case Filetypes::Begin_:
    return "Filetypes";
  case Filetypes::openQCD:
    return "openqcd";
  case Filetypes::QDP:
    return "qdp";
  default:
    throw std::runtime_error{"Invalid filetype passed to to_string"};
  };
}

std::vector<std::string> valid_filetypes() { return list_enums<Filetypes>(); }

} // namespace oniconverter
} // namespace fastsum
