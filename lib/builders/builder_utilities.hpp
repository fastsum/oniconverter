
/*
 * Created: 26-05-2018
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef BUILDER_UTILITIES_HPP
#define BUILDER_UTILITIES_HPP

#include <string>
#include <vector>

namespace fastsum {
namespace oniconverter {

enum class Filetypes
{
  Begin_,
  openQCD,
  QDP,
  End_,
};

std::string to_string(Filetypes ft);
Filetypes to_filetype(std::string ft_str);
std::vector<std::string> valid_filetypes();

} // namespace oniconverter
} // namespace fastsum

#endif /* BUILDER_UTILITIES_HPP */
